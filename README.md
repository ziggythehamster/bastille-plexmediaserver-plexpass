## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/plexmediaserver-plexpass/badges/master/pipeline.svg)](https://gitlab.com/bastillebsd-templates/plexmediaserver-plexpass/commits/master)


## plexmediaserver-plexpass
Bastille template for PlexMediaServer PlexPass

## Bootstrap

```shell
ishmael ~ # bastille bootstrap https://gitlab.com/bastillebsd-templates/plexmediaserver-plexpass
```

## Usage

```shell
ishmael ~ # bastille template TARGET bastillebsd-templates/plexmediaserver-plexpass
```
